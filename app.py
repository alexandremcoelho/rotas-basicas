from flask import Flask
import datetime as dt
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def hello_world():
    return {'data': 'Hello Flask!'}

@app.route('/current_datetime')
def hello():
    data_atual = dt.date.today()
    now = datetime.now()
    final = 'AM'
    if(int(now.hour) >= 12 ):
        final = 'PM'

    print(now)
    data = f'{data_atual.day}/{data_atual.month}/{data_atual.year} {now.hour}:{now.minute}:{now.second} {final}'
    msg = 'Bom dia!'
    if(int(now.hour) >= 12):
        msg = 'Boa tarde!'
    if(int(now.hour) >= 18):
        msg = 'Boa noite!'

    
    return {
        'current_datetime': data,
        'message': msg
        }